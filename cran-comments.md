## Resubmission
This a resubmission of winputall 1.0.0. I fix the problem by commenting out 
the lines that display information using cat() and that aren't necessary. 
For the display of important information I've used the message() function instead.
In Description, I change the version in 1.0.1. 
The evolution of the estimation process is displayed graphically at the bottom 
of the screen but the user can choose whether or not to display it by
set "showProgress = FALSE" in argument saem_control.

Thanks!

 > You write information messages to the console that cannot be easily suppressed.
 It is more R like to generate objects that can be used to extract the information
 a user is interested in, and then print() that object. 
 Instead of cat() rather use message()/warning() or if(verbose)cat(..) (or maybe stop()) 
 if you really have to write text to the console. 
 (except for print, summary, interactive functions) -> R/rpinpallEst_prog.R

## R CMD check results

0 errors | 0 warnings | 1 note

* This is a new release.

 
